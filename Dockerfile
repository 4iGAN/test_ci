FROM alpine:latest

RUN apk update
# INSTALL PYTHON
RUN apk add --no-cache \
    htop \
    vim \
    openssh \
    tmux \
    python3

# BUILD AND INSTALL psycopg2
RUN apk add --no-cache \
    musl-dev \
    python3-dev \
    postgresql-dev \
    gcc

RUN pip3 install \
    psycopg2

RUN apk del \
    musl-dev \
    python3-dev \
    postgresql-dev \
    gcc

# INSTALL NODEJS, GULP
RUN apk add --no-cache nodejs
RUN npm i gulp -g

CMD python3.5 -c 'print("HELLO WORLD")'

# INSTALL NGINX AND UWSGI
RUN apk add --no-cache nginx uwsgi
